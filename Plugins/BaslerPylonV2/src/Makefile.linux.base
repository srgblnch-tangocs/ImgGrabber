#=============================================================================
#
# file :        Makefile.h
#
# description : Makefile for DeviceServer
#
#
#=============================================================================
ifndef MAKE_ENV
    TANGO_HOME= $(shell pkg-config --variable prefix tango)
    MAKE_ENV = $(TANGO_HOME)/share/pogo/preferences/
endif

# TODO: pylon will be installed/linked in /opt
ifndef PYLON_ROOT
    PYLON_ROOT = /opt/pylon5/
endif

#=============================================================================
# RELEASE_TYPE
#=============================================================================
# - DEBUG     : debug symbols - no optimization
# - OPTIMIZED : no debug symbols - optimization level set to O2
#-----------------------------------------------------------------------------
ifndef RELEASE_TYPE
    RELEASE_TYPE = OPTIMIZED
endif


#=============================================================================
# OUTPUT_TYPE can be one of the following :
#   - 'STATIC_LIB' for a static library (.a)
#   - 'SHARED_LIB' for a dynamic library (.so)
#   - 'DEVICE' for a device server (will automatically include and link
#            with Tango dependencies)
#   - 'SIMPLE_EXE' for an executable with no dependency (for exemple the test tool
#                of a library with no Tango dependencies)
#
ifndef OUTPUT_TYPE
    OUTPUT_TYPE = SHARED_LIB
endif

#=============================================================================
# OUTPUT_DIR  is the directory which contains the build result.
# if not set, the standard location is :
#	- $HOME/DeviceServers if OUTPUT_TYPE is DEVICE
#	- ../bin for others
#
ifndef OUTPUT_DIR
    OUTPUT_DIR = .
endif

#=============================================================================
# PACKAGE_NAME is the name of the library/device/exe you want to build
#   - for a device server, PACKAGE_NAME will be prefixed by 'ds_'
#   - for a library (static or dynamic), PACKAGE_NAME will be prefixed by 'lib'
#   - for a simple executable, PACKAGE_NAME will be the name of the executable
#
PACKAGE_NAME = GrabPlugin_Basler

#=============================================================================
# INC_DIR_USER is the list of all include path needed by your sources
#   - for a device server, tango dependencies are automatically appended
#   - '-I ../include' and '-I .' are automatically appended in all cases
#
# -I$(SOLEIL_ROOT)/hw-support/ace/include for ACE library
# -I$(SOLEIL_ROOT)/hw-support/asl/include for ASL library
# -I$(SOLEIL_ROOT)/sw-support/hkl/include for HKL library
# ...etc
#

INC_DIR_USER =  -I../../../ImgGrabberSL/include \
                -I$(shell pkg-config --variable includedir yat) \
                -I$(PYLON_ROOT)/include \
                -I$(PYLON_ROOT)/include/GenApi \
                -I$(PYLON_ROOT)/include/Base \
                -I$(PYLON_ROOT)/include/pylon \
                -I$(PYLON_ROOT)/genicam/library/CPP/include

#=============================================================================
# LIB_DIR_USER is the list of user library directories
#   - for a device server, tango libraries directories are automatically appended
#   - '-L ../lib' is automatically appended in all cases
# -L $(SOLEIL_ROOT)/hw-support/ace/lib for ACE library
# -L $(SOLEIL_ROOT)/hw-support/asl/lib for ASL library
# -L $(SOLEIL_ROOT)/sw-support/hkl/lib for HKL library
# ...etc
#

ifeq ($(shell uname -m | grep _64$ | wc -w),1)
    LIB_DIR_USER= -L $(PYLON_ROOT)/lib64
else
    LIB_DIR_USER= -L $(PYLON_ROOT)/lib
endif


#=============================================================================
# LFLAGS_USR is the list of user link flags
#   - for a device server, tango libraries directories are automatically appended
#   - '-ldl -lpthread' is automatically appended in all cases
#
# !!! ATTENTION !!!
# Be aware that the order matters. 
# For example if you must link with libA, and if libA depends itself on libB
# you must use '-lA -lB' in this order as link flags, otherwise you will get
# 'undefined reference' errors
#
# -lACE for ACE
# -lASL for ASL
# -lHKL for HKL
#

ifeq ($(PYLON_ROOT) | grep pylon5$ | wc -w),1)
    # -lpylongigesupp does not exist in Pylon5
    LFLAGS_USR = -lpylonbase
else
    LFLAGS_USR = -lpylonbase -lpylonbase
endif


#=============================================================================
# CXXFLAGS_USR lists the compilation flags specific for your library/device/exe
# This is the place where to put your compile-time macros using '-Dmy_macro'
#
# -DACE_HAS_EXCEPTIONS -D__ACE_INLINE__ for ACE
#

CXXFLAGS_USR+=

ifeq ($(shell getos | grep _64$ | wc -w),1)
    CXXFLAGS_USER += -D__64BITS_PLATFORM__
endif

#=============================================================================
# TANGO_REQUIRED 
#=============================================================================
# - TRUE  : your project depends on TANGO
# - FALSE : your project does not depend on TANGO
#-----------------------------------------------------------------------------
# - NOTE : if PROJECT_TYPE is set to DEVICE, TANGO will be auto. added
#-----------------------------------------------------------------------------  
TANGO_REQUIRED = FALSE

#
#	include Standard TANGO compilation options
#
include $(MAKE_ENV)/tango.opt


#=============================================================================
# SVC_OBJS is the list of all objects needed to make the output
#
LIB_OBJS = 	    $(OBJDIR)/BaslerGrabber.o


#
# Verbose mode
#
.SILENT:

#
#	include common targets
#
include $(MAKE_ENV)/common_target.opt
